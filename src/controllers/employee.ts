import Router from 'express';
import { EMPLOYEE_SCHEMA } from '../dbmanager/modals/employee';
const router = Router();// router level 


router.post("/create", async (req, res) => {
    let body = req.body;
    // let newBody = {
    //     name: body.firstname,
    //     addr: body.address,
    //     mobileno: body.contact,
    //     companyname: body.companyname,
    //     loc: body.location
    // }
    let newEmployee = new EMPLOYEE_SCHEMA(body);
    newEmployee.save().then(result => {
        res.send({ status: 200, message: "Success" })
    }).catch(error => {
        res.send({ status: 400, message: "error" })
    })

});

router.post("/getemployee", async (req, res) => {
    let employee = await EMPLOYEE_SCHEMA.findOne({ mobileno: req.body.mno });
    res.send(employee)
});

router.post("/list", async (req, res) => {
    let employeeList = await EMPLOYEE_SCHEMA.find({ adminid: req.body.adminid });
    res.send(employeeList)
});


router.post("/updateOne", async (req, res) => {
    try {
        let employee = await EMPLOYEE_SCHEMA.findOne({ _id: req.body._id });
        if (employee) {
            EMPLOYEE_SCHEMA.updateOne({ _id: req.body._id },
                {
                    $set: {
                        name: req.body.name,
                        addr: req.body.addr
                    }
                }).then(result => {
                    res.send({ status: 200, message: "Employee details updated" });
                }).catch(error => {
                    res.send({ status: 400, message: "Something went wrong" });
                });
        } else {
            res.send({ status: 204, message: "Employee not exists" });
        }
    } catch (error) {
        res.send({ status: 400, message: "Something went wrong" });
    }
});


router.post("/updateMany", async (req, res) => {
    try {
        let employee = await EMPLOYEE_SCHEMA.findOne({ companyname: req.body.companyname });
        if (employee) {
            EMPLOYEE_SCHEMA.updateMany({ companyname: req.body.companyname },
                {
                    $set: {
                        loc: req.body.loc
                    }
                }).then(result => {
                    res.send({ status: 200, message: "Employee details updated" });
                }).catch(error => {
                    res.send({ status: 400, message: "Something went wrong" });
                });
        } else {
            res.send({ status: 204, message: "Company name not exists" });
        }
    }
    catch (error) {
        res.send({ status: 400, message: "Something went wrong" });
    }
});

// updateOne
// updateMany
// deleteOne
// deleteMany


export =router;



