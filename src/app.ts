import express from 'express';
const app = express(); // application level
import employeeRoute from './controllers/employee';
import studentRoute from './controllers/student';
import { mongoDBConnection } from './dbmanager/mongoconnection';


mongoDBConnection();

app.use(express.json())

const PORT = 3000;
app.listen(PORT, () => {
    console.log("Listing on port:" + PORT);
});

app.use("/employee", employeeRoute);

app.use((req, res, next) => {
    console.log("use method");
    if (true) {
        next();
    } else {
        res.send("token issue")
    }
});


app.use("/student", studentRoute);

// app.get("/example", (req, res) => {
//     res.send("Node js training");
// })

// app.post("/example", (req, res) => {
//     res.send({ status: 200, msg: "training" })
// });
