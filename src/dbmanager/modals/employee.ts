import mongoose from 'mongoose';

const employeeSchema = new mongoose.Schema({
    name: { type: String },
    addr: { type: String },
    mobileno: { type: String },
    companyname: { type: String },
    loc: { type: String }
});

export var EMPLOYEE_SCHEMA = mongoose.model("employees", employeeSchema)