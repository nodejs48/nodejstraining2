import mongoose from 'mongoose';

export function mongoDBConnection() {
    mongoose.connect('mongodb://localhost:27017/atpl', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    }).then(result => {
        console.log("Mongo db connected");
    }).catch(error => {
        console.log("Mongo db error" + error.message);
        setTimeout(() => {
            mongoDBConnection();
        }, 10000);
    });
}


//mongodb://localhost:27017/atpl
//mongodb://username:password@ip:port/databasename